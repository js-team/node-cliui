# Installation
> `npm install --save @types/pkgjs__parseargs`

# Summary
This package contains type definitions for @pkgjs/parseargs (https://github.com/pkgjs/parseargs).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/pkgjs__parseargs.

### Additional Details
 * Last updated: Tue, 07 Nov 2023 09:09:39 GMT
 * Dependencies: none

# Credits
These definitions were written by [Remco Haszing](https://github.com/remcohaszing).
